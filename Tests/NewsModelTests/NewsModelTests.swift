import XCTest
@testable import NewsModel

final class NewsModelTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(NewsModel().text, "Hello, World!")
    }
}
