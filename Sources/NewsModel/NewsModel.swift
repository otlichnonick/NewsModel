//
//  NewsModel.swift
//  SecondHomeworkApp
//
//  Created by Anton Agafonov on 05.07.2022.
//

import Foundation

public struct NewsModel: Codable {
    public var meta: MetaModel
    public var data: [DataModel]
    
    public init(meta: MetaModel, data: [DataModel]) {
        self.meta = meta
        self.data = data
    }
}

extension NewsModel {
    public init() {
        self.meta = MetaModel()
        self.data = [DataModel]()
    }
}

public struct MetaModel: Codable {
    public var found: Int = 0
    public var page: Int = 0
}

public struct DataModel: Codable, Equatable {
    public var uuid: String
    public var title: String
    public var description: String?
    public var image_url: String?
    
    public init(uuid: String, title: String, description: String?, image_url: String?) {
        self.uuid = uuid
        self.title = title
        self.description = description
        self.image_url = image_url
    }
}

extension DataModel {
    public init() {
        self.uuid = ""
        self.title = ""
    }
}
